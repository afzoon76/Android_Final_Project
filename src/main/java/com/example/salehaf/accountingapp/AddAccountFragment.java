package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by salehaf on 1/13/2018.
 */

public class AddAccountFragment extends BackHandledFragment {

    private Context context;
    private View view;
    private EditText accountName;
    private EditText accountBalance;
    private EditText accountDesc;
    private Button accountAdder;

    private AddAccountFragListener activityCommander;
    private DbHandler dbHandler;
    private TextView title;

    public interface AddAccountFragListener{

        void backToAccountsFrag();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AddAccountFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.add_account_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        title = (TextView)view.findViewById(R.id.add_account_title);
        title.setTypeface(MainActivity.boldFont);
        accountName = (EditText)view.findViewById(R.id.account_name_input);
        accountBalance = (EditText)view.findViewById(R.id.account_balance_input);
        accountDesc = (EditText) view.findViewById(R.id.account_desc_input);
        accountAdder = (Button)view.findViewById(R.id.add_account_button);

        accountAdder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(accountName.getText().toString().trim().equals("") ||
                        accountBalance.getText().toString().trim().equals(""))
                    Toast.makeText(context,"اطلاعات مورد نیاز را وارد کنید",Toast.LENGTH_LONG).show();
                else {

                }
                    Account account = new Account();
                    account.setName(accountName.getText().toString());
                    account.setDescription(accountDesc.getText().toString());

                try {
                        account.setBalance(Integer.valueOf(accountBalance.getText().toString()));

                    }catch (NumberFormatException e){

                    Toast.makeText(context,"موجودی حساب به درستی وارد نشده",Toast.LENGTH_LONG).show();
                    return;
                }
                dbHandler.addAccount(account);
                activityCommander.backToAccountsFrag();

            }

        });
        return view;
    }

    @Override
    public boolean onBackPressed() {

        activityCommander.backToAccountsFrag();

        return true;
    }

}
