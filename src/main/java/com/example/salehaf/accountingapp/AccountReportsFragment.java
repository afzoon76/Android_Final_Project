package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import saman.zamani.persiandate.PersianDate;

/**
 * Created by salehaf on 1/27/2018.
 */

public class AccountReportsFragment extends BackHandledFragment implements
ReportFragment.ReportFragmentListener{

    private Context context;
    private View view;
    private AccountReportsListener activityCommander;
    private DbHandler dbHandler;
    private FrameLayout accountReports;

    private TextView title;

    private SpinnerAdapter<Account> accountChooserAdapter;
    private Spinner accountChooser;

    private int accountId;

    private ReportFragment reportFragment;

    public Fragment getReportFragment() {
        if (reportFragment == null)
            reportFragment = new ReportFragment();
        return reportFragment;
    }

    @Override
    public void getReports(PersianDate date) {

        dbHandler.getAccountTransactions(this.accountId,date);

    }


    public interface AccountReportsListener{

    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AccountReportsListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();

        view = inflater.inflate(R.layout.account_rep_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);

        title = (TextView)view.findViewById(R.id.account_rep_title);
        title.setTypeface(MainActivity.boldFont);
        accountChooser = (Spinner)view.findViewById(R.id.origin_transfer_chooser);

        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if(view.findViewById(R.id.account_reports) != null) {
            transaction.add(R.id.container,getReportFragment());
            transaction.commit();
        }

        accountChooserAdapter = new SpinnerAdapter<>(Account.class,context,dbHandler.getAccounts());
        accountChooser.setAdapter(accountChooserAdapter);

        accountChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                accountId = ((Account)parent.getItemAtPosition(position)).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    @Override
    public boolean onBackPressed() {
        getActivity().onBackPressed();
        return true;
    }
}
