package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/14/2018.
 */

public class CategoryChooseAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<JuridicalCategory> categories;

    public CategoryChooseAdapter(Context context, ArrayList<JuridicalCategory> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_item,null);
        }

        TextView categoryName = (TextView)convertView.findViewById(R.id.category_name);
        categoryName.setTypeface(MainActivity.regFont);
        categoryName.setText(categories.get(position).getName());

        return convertView;
    }

}
