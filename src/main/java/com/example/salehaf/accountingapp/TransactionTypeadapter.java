package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/17/2018.
 */

public class TransactionTypeadapter extends BaseAdapter{

    private Context context;
    private String[] types;

    public TransactionTypeadapter(Context context, String[] types) {
        this.context = context;
        this.types = types;
    }

    @Override
    public int getCount() {
        return types.length;
    }

    @Override
    public Object getItem(int position) {
        return types[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_item,null);
        }

        TextView typeName = (TextView)convertView.findViewById(R.id.category_name);
        typeName.setTypeface(MainActivity.regFont);
        typeName.setText(types[position]);

        return convertView;
    }
}
