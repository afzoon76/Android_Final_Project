package com.example.salehaf.accountingapp;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by salehaf on 1/21/2018.
 */

public class CustomText extends android.support.v7.widget.AppCompatTextView {

    public CustomText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public CustomText(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        // Just Change your font name
        Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/Bahij_Nazanin_Regular.ttf");
        setTypeface(myTypeface);
    }
}