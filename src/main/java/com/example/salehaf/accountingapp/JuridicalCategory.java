package com.example.salehaf.accountingapp;

/**
 * Created by salehaf on 1/11/2018.
 */

public class JuridicalCategory implements NamedEntity{

    Integer id;
    String name;
    String description;

    public JuridicalCategory() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
