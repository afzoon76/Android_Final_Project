package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/19/2018.
 */

public class SpinnerAdapter<T extends NamedEntity> extends BaseAdapter {

    private Context context;
    private ArrayList<T> items;

    private final Class<T> clazz;

    public SpinnerAdapter(Class<T> clazz,Context context, ArrayList<T> items) {
        this.context = context;
        this.items = items;
        this.clazz = clazz;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_item,null);
        }

        TextView categoryName = (TextView)convertView.findViewById(R.id.category_name);
        categoryName.setTypeface(MainActivity.regFont);
        categoryName.setText(items.get(position).getName());

        return convertView;
    }

}
