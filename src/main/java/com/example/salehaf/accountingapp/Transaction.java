package com.example.salehaf.accountingapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import saman.zamani.persiandate.PersianDate;

/**
 * Created by salehaf on 1/11/2018.
 */

public class Transaction {

    private Integer id;
    private String type;
    private Integer firstCatID;
    private Integer secCatID;
    private Integer amount;
    private Integer accountID;
    private Integer juridicalID;
    private PersianDate date;
    private String description;
    private Integer photoID;

    public Transaction() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public PersianDate getDate() {
        return date;
    }

    public void setDate(PersianDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPhotoID() {
        return photoID;
    }

    public void setPhotoID(Integer photoID) {
        this.photoID = photoID;
    }

    public Integer getFirstCatID() {
        return firstCatID;
    }

    public void setFirstCatID(Integer firstCatID) {
        this.firstCatID = firstCatID;
    }

    public Integer getSecCatID() {
        return secCatID;
    }

    public void setSecCatID(Integer secCatID) {
        this.secCatID = secCatID;
    }

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }

    public Integer getJuridicalID() {
        return juridicalID;
    }

    public void setJuridicalID(Integer juridicalID) {
        this.juridicalID = juridicalID;
    }

    @Override
    public String toString() {
        return this.getType()+"/accountId:"+this.getAccountID()+"/jurId:"+this.getJuridicalID()
                +"/amount:"+this.getAmount() +"/firstCat:"+this.getFirstCatID()+"/SecCat:"+this.getSecCatID();
    }
}
