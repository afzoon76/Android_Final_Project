package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by salehaf on 1/12/2018.
 */

public class CategoryListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<JuridicalCategory>categories;
    private HashMap <JuridicalCategory,ArrayList<Juridical>> listHashMap;

    public CategoryListAdapter(Context context, ArrayList<JuridicalCategory> categories, HashMap<JuridicalCategory, ArrayList<Juridical>> listHashMap) {
        this.context = context;
        this.categories = categories;
        this.listHashMap = listHashMap;
    }

    @Override
    public int getGroupCount() {
        return categories.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listHashMap.get(categories.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return categories.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(categories.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        JuridicalCategory category = (JuridicalCategory)getGroup(groupPosition);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_header,null);
        }

        TextView categoryTitle = (TextView)convertView.findViewById(R.id.category_title);
        TextView categoryDesc = (TextView)convertView.findViewById(R.id.category_desc);

        categoryTitle.setTypeface(MainActivity.boldFont);
        categoryDesc.setTypeface(MainActivity.regFont);

        categoryTitle.setText(category.getName());
        categoryDesc.setText(category.getDescription());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        Juridical juridical = (Juridical)getChild(groupPosition,childPosition);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.juridical_item,null);
        }

        TextView juridicalName = (TextView)convertView.findViewById(R.id.juridical_name);
        TextView juridicalPhone = (TextView)convertView.findViewById(R.id.juridical_phone);

        juridicalName.setTypeface(MainActivity.boldFont);
        juridicalPhone.setTypeface(MainActivity.regFont);

        juridicalName.setText(juridical.getName());
        juridicalPhone.setText(juridical.getPhone());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
