package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

/**
 * Created by salehaf on 1/28/2018.
 */

public class ReportFragment extends Fragment {

    private Context context;
    private View view;
    private ReportFragmentListener activityCommander;
    private DbHandler dbHandler;

    private TextView strDate;

    private ListView reports;

    private Button prevMonthBtn;
    private Button prevDayBtn;
    private Button nextDayBtn;
    private Button nextMonthBtn;

    private PersianDate date;
    private PersianDateFormat dateFormat;
    private int accountId;


    public interface ReportFragmentListener{

        void getReports(PersianDate date);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (ReportFragmentListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.reports_panel, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        date = new PersianDate();
        dateFormat = new PersianDateFormat("Y/m/d");

        strDate = (TextView)view.findViewById(R.id.account_str_date);
        strDate.setText(dateFormat.format(date));

        prevMonthBtn = (Button)view.findViewById(R.id.prevMonth);
        prevDayBtn = (Button)view.findViewById(R.id.prevDay);
        nextDayBtn = (Button)view.findViewById(R.id.nextDay);
        nextMonthBtn = (Button)view.findViewById(R.id.nextMonth);

        reports = (ListView)view.findViewById(R.id.tran_rep_list);

        prevMonthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(date.getShMonth() == 1) {
                    date.setShMonth(12);
                    date.setShYear(date.getShYear() - 1);
                }
                else
                    date.setShMonth(date.getShMonth()-1);

                changeList(date);
            }
        });
        prevDayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(date.getShDay() == 1) {
                    date.setShDay(date.getMonthDays());
                    date.setShMonth(date.getShMonth()-1);
                }
                else
                    date.setShDay(date.getShDay()-1);

                changeList(date);
            }
        });
        nextDayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(date.getShDay() == date.getMonthDays()) {
                    date.setShDay(1);
                    date.setShMonth(date.getShMonth()+1);
                }
                else
                    date.setShDay(date.getShDay()+1);

                changeList(date);
            }
        });
        nextMonthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(date.getShMonth() == 12) {
                    date.setShMonth(1);
                    date.setShYear(date.getShYear() + 1);
                }
                else
                    date.setShMonth(date.getShMonth() + 1);

                changeList(date);
            }
        });

        return view;
    }

    private void changeList( PersianDate date){

        strDate.setText(dateFormat.format(date));
        activityCommander.getReports(date);
    }
}
