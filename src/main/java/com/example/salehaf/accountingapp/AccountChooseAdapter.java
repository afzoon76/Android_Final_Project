package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/15/2018.
 */

public class AccountChooseAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<Account> accounts;

    public AccountChooseAdapter(Context context, ArrayList<Account> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    @Override
    public int getCount() {
        return accounts.size();
    }

    @Override
    public Object getItem(int position) {
        return accounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.account_list_item,null);
        }

        TextView accountName = (TextView)convertView.findViewById(R.id.account_list_name);
        accountName.setTypeface(MainActivity.regFont);
        accountName.setText(accounts.get(position).getName());

        return convertView;
    }
}
