package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by salehaf on 1/15/2018.
 */

public class AddTransactionFragment extends BackHandledFragment {

    private Context context;
    private View view;
    private ListView accountsList;
    private AddTransactionFragListener activityCommander;
    private AccountAdaptor accountAdaptor;
    private DbHandler dbHandler;
    private TextView title;
    private Button addTransactionButton;
    private Button addTransactionCatButton;

    private TransactionTypeadapter transactionTypeAdapter;
    private Spinner transactionTypeChooser;

    private AccountChooseAdapter accountChooseAdapter;
    private Spinner accountChooser;

    private CategoryChooseAdapter categoryChooseAdapter;
    private Spinner categoryChooser;

    private JuridicalChooseAdapter juridicalChooseAdapter;
    private Spinner juridicalChooser;

    private TransactionCategoryAdapter transactionCategoryAdapter;
    private Spinner transactionFirstCatChooser;
    private Spinner transactionSecondCatChooser;

    private Transaction transaction;
    private EditText transactionAmount;

    private Account origin;

    public interface AddTransactionFragListener{

        void goToAddTransactionCatFrag();

    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AddTransactionFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.add_transaction_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        transactionAmount = (EditText)view.findViewById(R.id.transaction_amount_input);
        transaction = new Transaction();

        transactionTypeChooser = (Spinner)view.findViewById(R.id.transaction_type_chooser);
        categoryChooser = (Spinner)view.findViewById(R.id.target_category_chooser);
        juridicalChooser = (Spinner)view.findViewById(R.id.target_juridical_chooser);
        accountChooser = (Spinner)view.findViewById(R.id.target_account_chooser);
        transactionFirstCatChooser = (Spinner)view.findViewById(R.id.transaction_first_category_chooser);
        transactionSecondCatChooser = (Spinner)view.findViewById(R.id.transaction_second_category_chooser);

        String[] types = getResources().getStringArray(R.array.transaction_types);

        transactionTypeAdapter = new TransactionTypeadapter(context,types);
        transactionTypeChooser.setAdapter(transactionTypeAdapter);

        accountChooseAdapter = new AccountChooseAdapter(context,dbHandler.getAccounts());
        accountChooser.setAdapter(accountChooseAdapter);
        accountChooser.setPrompt("dasdas");

        transactionTypeChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                transactionCategoryAdapter = new TransactionCategoryAdapter(context,
                        dbHandler.getFirstLevelTransActionCats((String) parent.getItemAtPosition(position)));
                transactionFirstCatChooser.setAdapter(transactionCategoryAdapter);

                transaction.setType((String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        transactionFirstCatChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TransactionCategory category = (TransactionCategory)parent.getItemAtPosition(position);
                for(TransactionCategory cat:dbHandler.getSecLevelTransActionCats("هزینه",1))
                    Log.i("test",cat.getName());

                TransactionCategory transactionCategory = (TransactionCategory)parent.getItemAtPosition(position);
                SpinnerAdapter<TransactionCategory> cats =
                        new SpinnerAdapter<TransactionCategory>(TransactionCategory.class,context,
                                dbHandler.getSecLevelTransActionCats("هزینه",
                                        (transactionCategory.getId())));
                transactionCategoryAdapter = new TransactionCategoryAdapter(context,
                        dbHandler.getSecLevelTransActionCats(category.getType(),category.getId()));

                transactionSecondCatChooser.setAdapter(cats);

                transaction.setFirstCatID(transactionCategory.getId());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        transactionSecondCatChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TransactionCategory transactionCategory = (TransactionCategory)parent.getItemAtPosition(position);
                transaction.setSecCatID(transactionCategory.getId());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        accountChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                origin =((Account)parent.getItemAtPosition(position));
                transaction.setAccountID(origin.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        categoryChooseAdapter = new CategoryChooseAdapter(context,dbHandler.getJuridicalCats());
        categoryChooser.setAdapter(categoryChooseAdapter);
        categoryChooser.setPrompt("dasdas");
        categoryChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.i("test","jurs:"+dbHandler.getCategoryJuridicals((int)parent.getItemIdAtPosition(position)).toString());

                SpinnerAdapter<Juridical> jurAdpter = new SpinnerAdapter<Juridical>(Juridical.class,context,
                        dbHandler.getCategoryJuridicals( ((JuridicalCategory)parent.getItemAtPosition(position)).getId() ));

                Log.i("transaction",dbHandler.getCategoryJuridicals((int)parent.getItemIdAtPosition(position)).toString());
                juridicalChooser.setAdapter(jurAdpter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        juridicalChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                transaction.setJuridicalID( ((Juridical)parent.getItemAtPosition(position)).getId() );

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        JuridicalChooseAdapter adapter = new JuridicalChooseAdapter(context,dbHandler.getCategoryJuridicals(1));
        juridicalChooser.setAdapter(adapter);

        addTransactionButton = (Button) view.findViewById(R.id.add_transaction_button);
        addTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(transactionAmount.getText().toString().trim().equals(""))

                    Toast.makeText(context,"مبلغ تراکنش وارد نشده",Toast.LENGTH_SHORT).show();
                else {

                    try{

                        int amount = Integer.parseInt(transactionAmount.getText().toString());
                        if( transaction.getType()== "هزینه" && amount>origin.getBalance() )
                        {
                            Toast.makeText(context,"موجودی حساب کافی نیست",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        else
                            transaction.setAmount(amount);

                        try{
                            dbHandler.addTransaction(transaction);

                        }catch (SQLiteException e){
                            Toast.makeText(context,e.getMessage(),Toast.LENGTH_SHORT).show();
                        }finally {
                            origin.setBalance(origin.getBalance()-amount);
                        }

                    }catch (NumberFormatException e){
                        Toast.makeText(context,"مبلغ به درستی وارد نشده",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Log.i("test",transaction.toString());
                    dbHandler.addTransaction(transaction);

                }

                    }
        });

        addTransactionCatButton = (Button) view.findViewById(R.id.add_category_button);
        addTransactionCatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activityCommander.goToAddTransactionCatFrag();
            }
        });

        return view;
    }

    @Override
    public boolean onBackPressed() {
        getActivity().onBackPressed();
        return true;
    }
}
