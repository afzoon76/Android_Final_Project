package com.example.salehaf.accountingapp;

/**
 * Created by salehaf on 1/19/2018.
 */

public interface NamedEntity {
    String getName();
    void setName(String name);
}
