package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/15/2018.
 */

public class JuridicalChooseAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<Juridical> juridicals;

    public JuridicalChooseAdapter(Context context, ArrayList<Juridical> juridicals) {
        this.context = context;
        this.juridicals = juridicals;
    }

    @Override
    public int getCount() {
        return juridicals.size();
    }

    @Override
    public Object getItem(int position) {
        return juridicals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.juridical_list_item,null);
        }

        TextView juridicalName = (TextView)convertView.findViewById(R.id.juridi_list_name);
        juridicalName.setTypeface(MainActivity.regFont);
        juridicalName.setText(juridicals.get(position).getName());

        return convertView;
    }
}
