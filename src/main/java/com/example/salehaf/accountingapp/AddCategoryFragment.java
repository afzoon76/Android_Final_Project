package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by salehaf on 1/13/2018.
 */

public class AddCategoryFragment extends BackHandledFragment{

    private Context context;
    private View view;
    private EditText categoryName;
    private EditText categoryDesc;
    private Button categoryAdder;

    private AddCategoryFragListener activityCommander;
    private DbHandler dbHandler;
    private TextView title;

    public interface AddCategoryFragListener{

        void backToJuridicalsFrag();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AddCategoryFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.add_category_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        title = (TextView)view.findViewById(R.id.add_category_title);
        title.setTypeface(MainActivity.boldFont);
        categoryName = (EditText)view.findViewById(R.id.category_name_input);
        categoryDesc = (EditText)view.findViewById(R.id.category_desc_input);
        categoryAdder = (Button)view.findViewById(R.id.add_category_button);
        categoryAdder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(categoryName.getText().toString().trim().equals(""))
                    Toast.makeText(context,"اطلاعات مورد نیاز را وارد کنید",Toast.LENGTH_LONG).show();
                else {
                    JuridicalCategory category = new JuridicalCategory();
                    category.setName(categoryName.getText().toString());
                    category.setDescription(categoryDesc.getText().toString());
                    dbHandler.addJuridicalCat(category);

                    activityCommander.backToJuridicalsFrag();
                }
                    }
        });
        return view;
    }

    @Override
    public boolean onBackPressed() {

        if(getFragmentManager().getBackStackEntryCount() == 0) {
            getActivity().onBackPressed();
        }
        else {
            getFragmentManager().popBackStack();
        }return true;
    }

}
