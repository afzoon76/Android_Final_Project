package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by salehaf on 1/16/2018.
 */

public class SyncFragment extends BackHandledFragment {

    private Context context;
    private View view;
    private SyncFragListener activityCommander;
    private DbHandler dbHandler;
    private TextView title;
    private Button syncButton;

    public interface SyncFragListener{

        void goToAddAccountFragment();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (SyncFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.accounts_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        title = (TextView)view.findViewById(R.id.list_title);
        title.setTypeface(MainActivity.boldFont);
        syncButton = (Button)view.findViewById(R.id.sync_button);
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommander.goToAddAccountFragment();
            }
        });
        return view;
    }

    @Override
    public boolean onBackPressed() {
        getActivity().onBackPressed();
        return true;
    }
}
