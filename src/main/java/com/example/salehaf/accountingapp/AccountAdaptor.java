package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 12/29/2017.
 */

public class AccountAdaptor extends BaseAdapter {

    Context context;
    ArrayList<Account> accounts;
    static LayoutInflater inflater = null;

    public AccountAdaptor(Context context, ArrayList<Account> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    @Override
    public int getCount() {
        return accounts.size();
    }

    @Override
    public Object getItem(int position) {
        return getItemId(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        if(row == null){

            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.account_item,null);

        }
        TextView accountName = (TextView)row.findViewById(R.id.account_name);
        TextView accountBalance = (TextView)row.findViewById(R.id.account_balance);
        TextView accountDesc = (TextView)row.findViewById(R.id.account_desc);

        accountName.setText(accounts.get(position).getName());
        accountName.setTypeface(MainActivity.boldFont);
        accountBalance.setText(accounts.get(position).getBalance()+"");
        accountDesc.setText(accounts.get(position).getDescription());
        accountDesc.setTypeface(MainActivity.regFont);


        return row;
    }

}
