package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by salehaf on 1/26/2018.
 */

public class AddTransActionCatFragment extends BackHandledFragment {

    private Context context;
    private View view;
    private AddTransactionCatListener activityCommander;
    private DbHandler dbHandler;
    private TextView title;

    private TransactionTypeadapter transactionTypeAdapter;

    private TransactionCategoryAdapter transactionCategoryAdapter;

    private Spinner catTypeChooser;
    private Spinner parentCatChooser;

    private EditText parentCatName;
    private EditText childCatName;

    private Button addParentCatButton;
    private Button addChildCatButton;

    private TransactionCategory parentCategory;
    private TransactionCategory childCategory;

    public interface AddTransactionCatListener{

        void backToAddTransactionFrag();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AddTransactionCatListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        parentCategory = new TransactionCategory();
        childCategory = new TransactionCategory();
        view = inflater.inflate(R.layout.add_transaction_cat_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        title = (TextView)view.findViewById(R.id.add_trans_cat_title);

        title.setTypeface(MainActivity.boldFont);

        catTypeChooser = (Spinner) view.findViewById(R.id.transaction_type_chooser);
        parentCatChooser = (Spinner) view.findViewById(R.id.parent_cat_chooser);

        parentCatName = (EditText)view.findViewById(R.id.first_category_name_input);
        childCatName = (EditText)view.findViewById(R.id.sec_trans_category_name_input);

        addParentCatButton = (Button)view.findViewById(R.id.add_first_trans_cat_button);
        addChildCatButton = (Button)view.findViewById(R.id.add_sec_trans_cat_button);


        String[] types = getResources().getStringArray(R.array.transaction_types);

        transactionTypeAdapter = new TransactionTypeadapter(context,types);

        catTypeChooser.setAdapter(transactionTypeAdapter);

        catTypeChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                parentCategory.setType((String)parent.getItemAtPosition(position));
                childCategory.setType((String)parent.getItemAtPosition(position));

                transactionCategoryAdapter = new TransactionCategoryAdapter(context,
                        dbHandler.getFirstLevelTransActionCats((String) parent.getItemAtPosition(position)));

                parentCatChooser.setAdapter(transactionCategoryAdapter);

                addParentCatButton.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        parentCatChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                childCategory.setParentId( ((TransactionCategory)parent.getItemAtPosition(position)).getId());

              }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        addParentCatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

/*
                Log.i("transCat",category.getType());

                if(category.getType().equals(""))
                    Toast.makeText(context,"error",Toast.LENGTH_SHORT).show();
*/

                if(parentCatName.getText().toString().trim().equals("") ){

                    Toast.makeText(context,"اطلاعات کامل نیست",Toast.LENGTH_SHORT).show();

                }
                else {

                    parentCategory.setName(parentCatName.getText().toString());
                    dbHandler.addTransactionCat(parentCategory);
                    Toast.makeText(context,"دسته بندی اضافه شد",Toast.LENGTH_SHORT).show();
                    activityCommander.backToAddTransactionFrag();

                }

            }
        });

        addChildCatButton.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                if(childCatName.getText().toString().trim().equals("") ){

                    Toast.makeText(context,"اطلاعات کامل نیست",Toast.LENGTH_SHORT).show();

                }
                else {

                    childCategory.setName(childCatName.getText().toString());
                    dbHandler.addTransactionCat(childCategory);
                    Toast.makeText(context,"دسته بندی اضافه شد",Toast.LENGTH_SHORT).show();
                    activityCommander.backToAddTransactionFrag();
                }
            }
        });

        return view;
    }

    @Override
    public boolean onBackPressed() {
        getActivity().onBackPressed();
        return true;
    }

}

