package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static android.app.Activity.RESULT_OK;

/**
 * Created by salehaf on 1/13/2018.
 */

public class AddJuridicalFragment extends BackHandledFragment {

    private static final int PICK_CONTACT = 1 ;
    private Context context;
    private View view;
    private EditText juridicalName;
    private EditText juridicalPhone;
    private EditText juridicalEmail;
    private EditText juridicalDesc;
    private Spinner jurCatChooser;
    private CategoryChooseAdapter adapter;

    private Button juridicalAdder;
    private Button juridicalInsert;

    private AddJuridicalFragListener activityCommander;
    private DbHandler dbHandler;
    private TextView title;
    private Juridical juridical;

    public interface AddJuridicalFragListener{

        void backToJuridicalsFrag();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AddJuridicalFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.add_juridical_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        title = (TextView)view.findViewById(R.id.add_juridical_title);
        title.setTypeface(MainActivity.boldFont);

        juridicalName = (EditText)view.findViewById(R.id.juridical_name_input);
        juridicalPhone = (EditText)view.findViewById(R.id.juridical_phone_input);
        juridicalEmail = (EditText)view.findViewById(R.id.juridical_email_input);
        juridicalDesc = (EditText)view.findViewById(R.id.juridical_desc_input);

        juridicalAdder = (Button)view.findViewById(R.id.add_juridical_button);
        juridicalInsert = (Button)view.findViewById(R.id.insert_juridical_button);

        jurCatChooser = (Spinner)view.findViewById(R.id.jur_category_chooser);
        adapter = new CategoryChooseAdapter(context,dbHandler.getJuridicalCats());
        jurCatChooser.setAdapter(adapter);
        jurCatChooser.setPrompt("dasdas");
        juridical = new Juridical();

        jurCatChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                JuridicalCategory category = (JuridicalCategory) parent.getItemAtPosition(position);
                juridical.setCategoryId(category.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        juridicalAdder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(juridicalName.getText().toString().trim().equals(""))
                    Toast.makeText(context,"اطلاعات مورد نیاز را وارد کنید",Toast.LENGTH_LONG).show();


                else{

                    juridical.setName(juridicalName.getText().toString());
                    juridical.setPhone(juridicalPhone.getText().toString());
                    juridical.setEmail(juridicalEmail.getText().toString());
                    juridical.setDescription(juridicalDesc.getText().toString());

                    dbHandler.addJuridical(juridical);
                    activityCommander.backToJuridicalsFrag();

                    Toast.makeText(context,"مخاطب به لیست افزوده شد.",Toast.LENGTH_SHORT).show();
                }

            }
        });
        juridicalInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_CONTACT:
                    contactPicked(data);
                    break;
            }

        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            Uri uri = data.getData();
            cursor = context.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();

            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);

            Toast.makeText(context,name+"/"+phoneNo,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
