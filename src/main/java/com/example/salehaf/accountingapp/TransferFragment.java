package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by salehaf on 1/26/2018.
 */

public class TransferFragment extends BackHandledFragment {


    private Context context;
    private View view;
    private TransferFragListener activityCommander;
    private AccountChooseAdapter accountChooseAdapter;
    private DbHandler dbHandler;
    private TextView title;

    private EditText amountInput;
    private EditText transferDesc;

    private Spinner originChooser;
    private Spinner destChooser;

    private Button transferButton;

    private Transfer transfer;
    private Account origin;
    private Account dest;

    public interface TransferFragListener{

    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (TransferFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        transfer = new Transfer();
        view = inflater.inflate(R.layout.transfer_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        accountChooseAdapter = new AccountChooseAdapter(context,dbHandler.getAccounts());

        title = (TextView)view.findViewById(R.id.transfer_title);
        title.setTypeface(MainActivity.boldFont);

        amountInput = (EditText)view.findViewById(R.id.transfer_amount_input);
        transferDesc = (EditText)view.findViewById(R.id.transfer_desc_input);

        originChooser = (Spinner)view.findViewById(R.id.origin_transfer_chooser);
        destChooser = (Spinner)view.findViewById(R.id.dest_transfer_chooser);

        originChooser.setAdapter(accountChooseAdapter);
        destChooser.setAdapter(accountChooseAdapter);

        originChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                origin = ((Account)parent.getItemAtPosition(position));
                transfer.setOriginAccountID( origin.getId() );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        destChooser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dest = ((Account)parent.getItemAtPosition(position));
                transfer.setDestAccountID( dest.getId() );

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        transferButton = (Button)view.findViewById(R.id.transfer_button);
        transferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(transfer.getOriginAccountID() == transfer.getDestAccountID()) {
                    Toast.makeText(context, "حساب مبدا و مقصد یکسان هستند", Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    try {

                        int amount = Integer.parseInt(amountInput.getText().toString());
                        if (amount > origin.getBalance())
                        {
                            Toast.makeText(context,"موجودی حساب کافی نیست",Toast.LENGTH_SHORT).show();
                            return;
                        }

                        transfer.setAmount(amount);
                        transfer.setDescription(transferDesc.getText().toString());

                        try{
                            dbHandler.addTransfer(transfer);

                        }catch (SQLiteException e){
                            Toast.makeText(context,e.getMessage(),Toast.LENGTH_SHORT).show();
                        }finally {
                            origin.setBalance(origin.getBalance()-amount);
                            dest.setBalance(dest.getBalance()+amount);
                        }

                    }catch (NumberFormatException e){

                        Toast.makeText(context,"مبلغ انتقال به درستی وارد نشده",Toast.LENGTH_SHORT).show();
                        return;
                    }

                }
            }
        });
        return view;
    }

    @Override
    public boolean onBackPressed() {
        getActivity().onBackPressed();
        return true;
    }

}
