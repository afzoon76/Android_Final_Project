package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.view.menu.MenuAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by salehaf on 1/14/2018.
 */

public class MenuFragment extends BackHandledFragment {

    private Context context;
    private View view;
    MenuFragListener activityCommander;
    private DbHandler dbHandler;
    private TextView title;

    private String[] menuItemNames;
    private TypedArray menuItemsImage;
    private MenuListAdapter menuAdapter;
    private GridView menu;

    public interface MenuFragListener{

        void launchFragment(int fragNumber);
        void backToJuridicalsFrag();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (MenuFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.menu_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        title = (TextView)view.findViewById(R.id.option_title);
        title.setTypeface(MainActivity.boldFont);
        menu = (GridView)view.findViewById(R.id.menu_list);

        menuItemNames = getResources().getStringArray(R.array.menu_items_names);
        menuItemsImage = getResources().obtainTypedArray(R.array.menu_items_icons);

        menuAdapter = new MenuListAdapter(context,menuItemNames,menuItemsImage);
        menu.setAdapter(menuAdapter);
        menu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
        menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                activityCommander.launchFragment((int)parent.getItemIdAtPosition(position));
            }
        });
        return view;
    }
    @Override
    public boolean onBackPressed() {
        return false;
    }
}
