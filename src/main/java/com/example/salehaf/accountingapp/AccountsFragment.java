package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by salehaf on 1/10/2018.
 */

public class AccountsFragment extends BackHandledFragment {

    private Context context;
    private View view;
    private ListView accountsList;
    private AccountsFragListener activityCommander;
    private AccountAdaptor accountAdaptor;
    private DbHandler dbHandler;
    private TextView title;
    private Button addAccountButton;

    public interface AccountsFragListener{

        void goToAddAccountFragment();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (AccountsFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.accounts_fragment, container, false);
        dbHandler = DbHandler.getDbInstance(context);
        accountAdaptor = new AccountAdaptor(context,dbHandler.getAccounts());
        accountsList = (ListView)view.findViewById(R.id.accounts_list);
        accountsList.setAdapter(accountAdaptor);
        title = (TextView)view.findViewById(R.id.list_title);
        title.setTypeface(MainActivity.boldFont);
        addAccountButton = (Button)view.findViewById(R.id.add_account_frag_button);
        addAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommander.goToAddAccountFragment();
            }
        });
        return view;
    }

    @Override
    public boolean onBackPressed() {
        getActivity().onBackPressed();
        return true;
    }

}
