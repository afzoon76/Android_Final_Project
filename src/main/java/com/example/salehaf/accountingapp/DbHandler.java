package com.example.salehaf.accountingapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by salehaf on 1/10/2018.
 */
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


public class DbHandler extends SQLiteOpenHelper {

    private static final String TAG = "dbHandler";

    private static final String DATABASE_NAME = "AccountingDB";
    private static final int  DATABASE_VERSION = 8;

    private static final String TABLE_ACCOUNT = "ACCOUNTS";
    private static final String TABLE_JUR_CATEGORY = "JUR_CATEGORIES";
    private static final String TABLE_JURIDICAL = "JURIDICALS";
    private static final String TABLE_TRANSACTION_CATEGORY = "TRANSACTON_CATEGORY";
    private static final String TABLE_TRANSACTION = "TRANSACTONS";
    private static final String TABLE_TRANSFER = "TRANSFERS";

    private static final String ACCOUNT_ID = "id";
    private static final String ACCOUNT_NAME = "account";
    private static final String ACCOUNT_BALANCE = "balance";
    private static final String ACCOUNT_DESCRIPTION = "description";

    private static final String JUR_CATEGORY_ID = "id";
    private static final String JUR_CATEGORY_NAME = "name";
    private static final String JUR_CATEGORY_DESCRIPTION = "description";

    private static final String JURIDICAL_ID = "id";
    private static final String JURIDICAL_NAME = "juridical";
    private static final String JURIDICAL_PHONE = "phone";
    private static final String JURIDICAL_EMAIL = "email";
    private static final String JURIDICAL_DESCRIPTION = "description";
    private static final String JURDICAL_CAT_ID = "jur_category_id";

    private static final String CATEGORY_ID = "id";
    private static final String CATEGORY_NAME = "name";
    private static final String TRANSACTION_CAT_TYPE = "categoryType";
    private static final String PARENT_CAT_ID = "parentCatId";

    private static final String TRANSFER_ID = "id";
    private static final String ORIGIN_ACCOUNT_ID = "OriginAccountId";
    private static final String DEST_ACCOUNT_ID = "DestAccountId";
    private static final String TRANSFER_AMOUNT = "amount";
    private static final String TRANSFER_DATE = "date";
    private static final String TRANSFER_DESCRIPTION = "description";

    private static final String TRANSACTION_ID = "id";
    private static final String TRANSACTION_TYPE = "type";
    private static final String TRANSACTION_MAIN_CAT = "mainCategory";
    private static final String TRANSACTION_SUB_CAT = "subCategory";
    private static final String TRANSACTION_AMOUNT = "amount";
    private static final String TRANSACTION_ACCOUNT_ID = "AccountId";
    private static final String TRANSACTION_JUR_ID = "JuridicalId";
    private static final String TRANSACTION_DATE = "date";
    private static final String TRANSACTION_DESCRIPTION = "description";


    private static final String CREATE_ACCOUNT_TABLE =
            "CREATE TABLE "+ TABLE_ACCOUNT +"(" +
                    ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    ACCOUNT_NAME + " TEXT ,"+
                    ACCOUNT_BALANCE + " INTEGER ,"+
                    ACCOUNT_DESCRIPTION + " TEXT)";


    private static final String CREATE_JUR_CATEGORY_TABLE =
            "CREATE TABLE "+ TABLE_JUR_CATEGORY +"(" +
                    JUR_CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    JUR_CATEGORY_NAME + " TEXT ,"+
                    JUR_CATEGORY_DESCRIPTION + " TEXT)";


    private static final String CREATE_JURIDICAL_TABLE =
            "CREATE TABLE "+ TABLE_JURIDICAL +"(" +
                    JURIDICAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    JURIDICAL_NAME + " TEXT ,"+
                    JURIDICAL_PHONE + " TEXT ,"+
                    JURIDICAL_EMAIL + " TEXT ,"+
                    JURIDICAL_DESCRIPTION + " TEXT ," +
                    JURDICAL_CAT_ID + " INTEGER NOT NULL," +
                    " FOREIGN KEY ("+JURDICAL_CAT_ID+") REFERENCES "+TABLE_JUR_CATEGORY+"("+JUR_CATEGORY_ID+")" +
                    " ON DELETE CASCADE "+");";

    private static final String CREATE_TRANSACTION_CATEGORY_TABLE =
            "CREATE TABLE "+ TABLE_TRANSACTION_CATEGORY +"(" +
                    CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    CATEGORY_NAME + " TEXT,"+
                    TRANSACTION_CAT_TYPE + " TEXT,"+
                    PARENT_CAT_ID + " INTEGER)";

    private static final String CREATE_TRANSACTION_TABLE =
            "CREATE TABLE "+ TABLE_TRANSACTION +"(" +
                    TRANSACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    TRANSACTION_TYPE + " TEXT,"+
                    TRANSACTION_MAIN_CAT + " INTEGER,"+
                    TRANSACTION_SUB_CAT + " INTEGER,"+
                    TRANSACTION_AMOUNT + " INTEGER,"+
                    TRANSACTION_ACCOUNT_ID + " INTEGER,"+
                    TRANSACTION_JUR_ID + " INTEGER,"+
                    TRANSACTION_DATE + " INTEGER,"+
                    TRANSACTION_DESCRIPTION + " TEXT,"+
            " FOREIGN KEY ("+TRANSACTION_MAIN_CAT+") REFERENCES "+TABLE_TRANSACTION_CATEGORY
                    +"("+CATEGORY_ID+")" + " ON DELETE CASCADE ,"+
            " FOREIGN KEY ("+TRANSACTION_SUB_CAT+") REFERENCES "+TABLE_TRANSACTION_CATEGORY
                    +"("+CATEGORY_ID+")" + " ON DELETE CASCADE ,"+
            " FOREIGN KEY ("+TRANSACTION_ACCOUNT_ID+") REFERENCES "+TABLE_ACCOUNT
                    +"("+ACCOUNT_ID+")" + " ON DELETE CASCADE ,"+
            " FOREIGN KEY ("+TRANSACTION_JUR_ID+") REFERENCES "+TABLE_JURIDICAL
                    +"("+JURIDICAL_ID+")" + " ON DELETE CASCADE);";

    private static final String CREATE_TRANSFER_TABLE =
            "CREATE TABLE "+ TABLE_TRANSFER +"(" +
                    TRANSFER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    TRANSFER_AMOUNT + " INTEGER,"+
                    ORIGIN_ACCOUNT_ID + " INTEGER,"+
                    DEST_ACCOUNT_ID + " INTEGER,"+
                    TRANSFER_DATE + " INTEGER,"+
                    TRANSFER_DESCRIPTION + " TEXT,"+
            " FOREIGN KEY ("+ORIGIN_ACCOUNT_ID+") REFERENCES "+TABLE_ACCOUNT
                    +"("+ACCOUNT_ID+")" + " ON DELETE CASCADE ,"+
            " FOREIGN KEY ("+DEST_ACCOUNT_ID+") REFERENCES "+TABLE_ACCOUNT
                    +"("+ACCOUNT_ID+")" + " ON DELETE CASCADE);";


    private static DbHandler instance;

    private DbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);

    }

    public static synchronized DbHandler getDbInstance(Context context){

        if(instance == null)
            instance = new DbHandler(context,null,null,1);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_ACCOUNT_TABLE);
        db.execSQL(CREATE_JUR_CATEGORY_TABLE);
        db.execSQL(CREATE_JURIDICAL_TABLE);
        db.execSQL(CREATE_TRANSACTION_CATEGORY_TABLE);
        db.execSQL(CREATE_TRANSACTION_TABLE);
        db.execSQL(CREATE_TRANSFER_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion!= newVersion){
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_JUR_CATEGORY);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_JURIDICAL);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION_CATEGORY);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSFER);
            onCreate(db);
        }
    }

    public void addAccount(Account account){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {

            ContentValues cv = new ContentValues();

            cv.put(ACCOUNT_NAME,account.getName());
            cv.put(ACCOUNT_BALANCE,account.getBalance());
            cv.put(ACCOUNT_DESCRIPTION,account.getDescription());

            Log.i(TAG,account.toString());

            account.setId((int)db.insertOrThrow(TABLE_ACCOUNT,null,cv));

            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while trying to add account to database");
        }finally {
            db.endTransaction();
        }

    }

    public Account getAccount(Integer accountID){

        Account account = new Account();
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " +TABLE_ACCOUNT + " WHERE id = '"+accountID+"'", null);
        c.moveToFirst();
        try {
            account.setId(c.getInt(c.getColumnIndex("id")));
            account.setName(c.getString(c.getColumnIndex("account")));
            account.setBalance(c.getInt(c.getColumnIndex("balance")));
            account.setDescription(c.getString(c.getColumnIndex("description")));
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.i("database:",account.getName()+"");

        db.close();
        return account;
    }

    public ArrayList<Account> getAccounts(){

        ArrayList<Account> accounts = new ArrayList<>();
        Account account;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_ACCOUNT;
        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    account = new Account();
                    account.setId(cursor.getInt(cursor.getColumnIndex(ACCOUNT_ID)));
                    account.setName(cursor.getString(cursor.getColumnIndex(ACCOUNT_NAME)));
                    account.setBalance(cursor.getInt(cursor.getColumnIndex(ACCOUNT_BALANCE)));
                    account.setDescription(cursor.getString(cursor.getColumnIndex(ACCOUNT_DESCRIPTION)));
                    accounts.add(account);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get accounts from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return accounts;
    }

    public void deleteAccount(Account account){

        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_ACCOUNT, ACCOUNT_ID + "=?", new String[]{String.valueOf(account.getId())});
        db.close();
    }

    public void addJuridicalCat(JuridicalCategory category){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {

            ContentValues cv = new ContentValues();

            cv.put(JUR_CATEGORY_NAME,category.getName());
            cv.put(JUR_CATEGORY_DESCRIPTION,category.getDescription());

            Log.i(TAG,category.toString());

            category.setId((int)db.insertOrThrow(TABLE_JUR_CATEGORY,null,cv));

            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while trying to add juridical category to database");
        }finally {
            db.endTransaction();
        }


    }

    public void addJuridical(Juridical juridical){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {

            ContentValues cv = new ContentValues();

            cv.put(JURIDICAL_NAME,juridical.getName());
            cv.put(JURIDICAL_PHONE,juridical.getPhone());
            cv.put(JURIDICAL_EMAIL,juridical.getEmail());
            cv.put(JURIDICAL_DESCRIPTION,juridical.getDescription());
            cv.put(JURDICAL_CAT_ID,juridical.getCategoryId());

            juridical.setId((int)db.insertOrThrow(TABLE_JURIDICAL,null,cv));

            Log.i(TAG,juridical.getId()+"");

            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }finally {
            db.endTransaction();
        }
    }

    public ArrayList<JuridicalCategory> getJuridicalCats(){

        ArrayList<JuridicalCategory> categories = new ArrayList<>();
        JuridicalCategory category;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_JUR_CATEGORY;
        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    category = new JuridicalCategory();
                    category.setId(cursor.getInt(cursor.getColumnIndex(JUR_CATEGORY_ID)));
                    category.setName(cursor.getString(cursor.getColumnIndex(JUR_CATEGORY_NAME)));
                    category.setDescription(cursor.getString(cursor.getColumnIndex(JUR_CATEGORY_DESCRIPTION)));

                    categories.add(category);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get juridicals category from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return categories;
    }

    public ArrayList<Juridical> getCategoryJuridicals(int categoryID){

        ArrayList<Juridical> juridicals = new ArrayList<>();
        Juridical juridical;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " +TABLE_JURIDICAL +" WHERE "
                        +JURDICAL_CAT_ID +"='"+categoryID +"'", null);
        cursor.moveToFirst();
        try {
            do {
                juridical = new Juridical();
                juridical.setId(cursor.getInt(cursor.getColumnIndex(JURIDICAL_ID)));
                juridical.setName(cursor.getString(cursor.getColumnIndex(JURIDICAL_NAME)));
                juridical.setPhone(cursor.getString(cursor.getColumnIndex(JURIDICAL_PHONE)));
                juridical.setEmail(cursor.getString(cursor.getColumnIndex(JURIDICAL_EMAIL)));
                juridical.setDescription(cursor.getString(cursor.getColumnIndex(JURIDICAL_DESCRIPTION)));

                Log.i(TAG,"juridical:"+juridical.getName());
                juridicals.add(juridical);

            }while (cursor.moveToNext());
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        db.close();

        return juridicals;
    }

    public ArrayList<Juridical> getJuridicals(){

        ArrayList<Juridical> juridicals = new ArrayList<>();
        Juridical juridical;
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " +TABLE_JURIDICAL,null);
        cursor.moveToFirst();
        try {
            do {
                juridical = new Juridical();
                juridical.setId(cursor.getInt(cursor.getColumnIndex(JURIDICAL_ID)));
                juridical.setName(cursor.getString(cursor.getColumnIndex(JURIDICAL_NAME)));
                juridical.setPhone(cursor.getString(cursor.getColumnIndex(JURIDICAL_PHONE)));
                juridical.setEmail(cursor.getString(cursor.getColumnIndex(JURIDICAL_EMAIL)));
                juridical.setCategoryId(cursor.getInt(cursor.getColumnIndex(JURDICAL_CAT_ID)));
                juridical.setDescription(cursor.getString(cursor.getColumnIndex(JURIDICAL_DESCRIPTION)));

                Log.i(TAG,"juridical:"+juridical.getName());
                juridicals.add(juridical);

            }while (cursor.moveToNext());
        }catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        db.close();

        return juridicals;
    }

    public void addTransactionCat(TransactionCategory category){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues cv = new ContentValues();

            cv.put(CATEGORY_NAME,category.getName());
            cv.put(PARENT_CAT_ID ,category.getParentId());
            cv.put(TRANSACTION_CAT_TYPE,category.getType());

            Log.i(TAG,category.toString());

            category.setId((int)db.insertOrThrow(TABLE_TRANSACTION_CATEGORY,null,cv));

            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while trying to add transaction category to database");
        }finally {
            db.endTransaction();
        }

    }

    public ArrayList<TransactionCategory> getFirstLevelTransActionCats(String transactionType){

        ArrayList<TransactionCategory> categories = new ArrayList<>();
        TransactionCategory category;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_TRANSACTION_CATEGORY
                +" WHERE "+TRANSACTION_CAT_TYPE +" = '" + transactionType + "'"+
                " AND "+ PARENT_CAT_ID +" IS NULL";

        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    category = new TransactionCategory();
                    category.setId(cursor.getInt(cursor.getColumnIndex(CATEGORY_ID)));
                    category.setName(cursor.getString(cursor.getColumnIndex(CATEGORY_NAME)));
                    categories.add(category);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get transaction categories from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return categories;
    }

    public ArrayList<TransactionCategory> getSecLevelTransActionCats(String transactionType,int parentID){

        ArrayList<TransactionCategory> categories = new ArrayList<>();
        TransactionCategory category;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_TRANSACTION_CATEGORY
                +" WHERE "+TRANSACTION_CAT_TYPE + " = '"+transactionType + "' AND "+
                PARENT_CAT_ID + " = "+ parentID;

        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    category = new TransactionCategory();
                    category.setId(cursor.getInt(cursor.getColumnIndex(CATEGORY_ID)));
                    category.setName(cursor.getString(cursor.getColumnIndex(CATEGORY_NAME)));
                    category.setParentId(cursor.getInt(cursor.getColumnIndex(PARENT_CAT_ID)));
                    categories.add(category);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get transaction categories from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return categories;
    }

    public void addTransaction(Transaction transaction){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues cv = new ContentValues();

            cv.put(TRANSACTION_TYPE,transaction.getType());
            cv.put(TRANSACTION_MAIN_CAT,transaction.getFirstCatID());
            cv.put(TRANSACTION_SUB_CAT,transaction.getSecCatID());
            cv.put(TRANSACTION_AMOUNT,transaction.getAmount());
            cv.put(TRANSACTION_ACCOUNT_ID,transaction.getAccountID());
            cv.put(TRANSACTION_JUR_ID,transaction.getJuridicalID());
            cv.put(TRANSACTION_DESCRIPTION,transaction.getDescription());


            Log.i(TAG,transaction.toString());

            transaction.setId((int)db.insertOrThrow(TABLE_TRANSACTION,null,cv));

            db.setTransactionSuccessful();

            db.execSQL("update "+TABLE_TRANSACTION + "set "+TRANSACTION_DATE +" = "+ transaction.getDate().getSecond()
                    + " where "+TRANSACTION_ID +" = "+transaction.getId());

        }catch (Exception e){
            Log.e(TAG,"Error while trying to add transaction to database");
        }finally {
            db.endTransaction();
        }

    }

    public ArrayList<Transaction> getTransactions(){

        ArrayList<Transaction> transactions = new ArrayList<>();
        Transaction transaction;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_TRANSACTION;
        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    transaction = new Transaction();
                    transaction.setId(cursor.getInt(cursor.getColumnIndex(TRANSACTION_ID)));
                    transaction.setType(cursor.getString(cursor.getColumnIndex(TRANSACTION_TYPE)));
                    transaction.setAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTION_AMOUNT)));
                    transaction.setAccountID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_ACCOUNT_ID)));
                    transaction.setJuridicalID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_JUR_ID)));
                    transaction.setFirstCatID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_MAIN_CAT)));
                    transaction.setSecCatID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_SUB_CAT)));
                    transaction.setDate(new PersianDate().setSecond( cursor.getInt(cursor.getColumnIndex(TRANSACTION_DATE)) ));
                    transaction.setDescription(cursor.getString(cursor.getColumnIndex(TRANSACTION_DESCRIPTION)));

                    transactions.add(transaction);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get transactions from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return transactions;
    }

    public ArrayList<Transaction> getAccountTransactions(int accountId ,PersianDate date){

        ArrayList<Transaction> transactions = new ArrayList<>();
        Transaction transaction;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_TRANSACTION + " where "+TRANSACTION_ACCOUNT_ID
                + " = "+accountId +" and "+
                TRANSACTION_DATE + " = "+ new PersianDateFormat("Y/m/d").format(date);
        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    transaction = new Transaction();
                    transaction.setId(cursor.getInt(cursor.getColumnIndex(TRANSACTION_ID)));
                    transaction.setType(cursor.getString(cursor.getColumnIndex(TRANSACTION_TYPE)));
                    transaction.setAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTION_AMOUNT)));
                    transaction.setAccountID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_ACCOUNT_ID)));
                    transaction.setJuridicalID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_JUR_ID)));
                    transaction.setFirstCatID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_MAIN_CAT)));
                    transaction.setSecCatID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_SUB_CAT)));
                    transaction.setDate(new PersianDate().setSecond( cursor.getInt(cursor.getColumnIndex(TRANSACTION_DATE)) ));
                    transaction.setDescription(cursor.getString(cursor.getColumnIndex(TRANSACTION_DESCRIPTION)));

                    transactions.add(transaction);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get transactions from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return transactions;
    }

    public void addTransfer(Transfer transfer) throws SQLiteException {

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues cv = new ContentValues();

            cv.put(TRANSFER_AMOUNT,transfer.getAmount());
            cv.put(ORIGIN_ACCOUNT_ID,transfer.getOriginAccountID());
            cv.put(DEST_ACCOUNT_ID,transfer.getDestAccountID());
            cv.put(TRANSFER_DESCRIPTION,transfer.getDescription());

            transfer.setId((int)db.insertOrThrow(TABLE_TRANSFER,null,cv));

            db.setTransactionSuccessful();

            db.execSQL("update "+TABLE_TRANSFER + "set "+TRANSFER_DATE +" = "+ transfer.getDate().getSecond()
                    + " where "+TRANSFER_ID +" = "+transfer.getId());

            Log.i(TAG,transfer.toString());

        }catch (SQLiteException e){
            throw new SQLiteException("faild to add transfer");
        }finally {
            db.endTransaction();
        }

    }

    public ArrayList<Transfer> getTransfers(){

        ArrayList<Transfer> transfers = new ArrayList<>();
        Transfer transfer;

        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM "+TABLE_TRANSFER;
        Cursor cursor = db.rawQuery(query,null);
        try {
            if(cursor.moveToFirst())
                do{
                    transfer = new Transfer();
                    transfer.setId(cursor.getInt(cursor.getColumnIndex(TRANSFER_ID)));
                    transfer.setAmount(cursor.getInt(cursor.getColumnIndex(TRANSFER_AMOUNT)));
                    transfer.setOriginAccountID(cursor.getInt(cursor.getColumnIndex(ORIGIN_ACCOUNT_ID)));
                    transfer.setDestAccountID(cursor.getInt(cursor.getColumnIndex(DEST_ACCOUNT_ID)));
                    transfer.setDate(new PersianDate().setSecond( cursor.getInt(cursor.getColumnIndex(TRANSFER_DATE)) ));
                    transfer.setDescription(cursor.getString(cursor.getColumnIndex(TRANSFER_DESCRIPTION)));

                    transfers.add(transfer);

                }while (cursor.moveToNext());
            else
                Log.i(TAG,"data base is empty");

        }catch (Exception e){
            Log.e(TAG,"Error while trying to get transfers from database");
        }finally {
            if(cursor!=null && !cursor.isClosed())
                cursor.close();
        }

        return transfers;
    }

    public void removeAccounts(){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_ACCOUNT,null,null);
            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while deleting account table");
        }finally {
            db.endTransaction();
        }
    }

    public void removeJuridicals(){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_JURIDICAL,null,null);
            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while deleting juridicals table");
        }finally {
            db.endTransaction();
        }
    }

    public void removeJuridicalsCats(){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_JUR_CATEGORY,null,null);
            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while deleting juridical categories table");
        }finally {
            db.endTransaction();
        }
    }

    public void removeTransactionCats(){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_TRANSACTION_CATEGORY,null,null);
            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while deleting transaction categories table");
        }finally {
            db.endTransaction();
        }
    }

    public void removeTransactions(){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_TRANSACTION,null,null);
            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while deleting transaction table");
        }finally {
            db.endTransaction();
        }
    }

    public void removeTransfers(){

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_TRANSFER,null,null);
            db.setTransactionSuccessful();

        }catch (Exception e){
            Log.e(TAG,"Error while deleting transfers table");
        }finally {
            db.endTransaction();
        }
    }
}
