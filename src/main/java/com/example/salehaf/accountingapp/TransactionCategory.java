package com.example.salehaf.accountingapp;

import java.util.HashMap;

/**
 * Created by salehaf on 1/16/2018.
 */

public class TransactionCategory implements NamedEntity{

    private HashMap <Integer,String> types;
    private Integer id;
    private Integer parentId;
    private String name;
    private String type;

    public TransactionCategory() {
        types = new HashMap<>();
        types.put(1,"هزینه");
        types.put(2,"درآمد");

    }

    public TransactionCategory(Integer id, String name) {

        this.id = id;
        this.name = name;
        types = new HashMap<>();
        types.put(1,"هزینه");
        types.put(2,"درآمد");
        parentId = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "parentId:"+this.getParentId()+"/name:"+this.getName();
    }
}
