package com.example.salehaf.accountingapp;

/**
 * Created by salehaf on 1/10/2018.
 */

public class Account implements NamedEntity{

    private Integer id;
    private String name;
    private Integer balance;
    private String description;

    public Account() {
    }

    public Account(Integer id, String name, Integer balance , String description) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
