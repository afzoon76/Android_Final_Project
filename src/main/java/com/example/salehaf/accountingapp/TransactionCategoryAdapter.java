package com.example.salehaf.accountingapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/17/2018.
 */

public class TransactionCategoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<TransactionCategory> categories;

    public TransactionCategoryAdapter(Context context, ArrayList<TransactionCategory> categories) {
        this.context = context;
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_item,null);
        }

        TextView juridicalName = (TextView)convertView.findViewById(R.id.category_name);
        juridicalName.setTypeface(MainActivity.regFont);
        juridicalName.setText(categories.get(position).getName());

        return convertView;
    }
}
