package com.example.salehaf.accountingapp;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class MainActivity extends AppCompatActivity implements BackHandledFragment.BackHandlerInterface
        ,AccountsFragment.AccountsFragListener,JuridicalsFragment.JuridicalsFragListener ,
        AddCategoryFragment.AddCategoryFragListener,AddAccountFragment.AddAccountFragListener,
        AddJuridicalFragment.AddJuridicalFragListener,MenuFragment.MenuFragListener,
        AddTransactionFragment.AddTransactionFragListener,AddTransActionCatFragment.AddTransactionCatListener
        ,TransferFragment.TransferFragListener,AccountReportsFragment.AccountReportsListener,
        ReportFragment.ReportFragmentListener{

    private static final String TAG = "main";
    public static Typeface regFont;
    public static Typeface boldFont;

    private BackHandledFragment selectedFragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private DbHandler dbHandler;

    private MenuFragment menuFrag = null;
    private AccountsFragment accountFrag = null;
    private TransferFragment transferFrag = null;
    private JuridicalsFragment juridicalFrag = null;
    private AddCategoryFragment addCatFrag = null;
    private AddAccountFragment addAccountFrag = null;
    private AddJuridicalFragment addJUridicalFrag = null;
    private AddTransactionFragment addTransactionFrag = null;
    private AddTransActionCatFragment addTransactionCatFrag = null;
    private AccountReportsFragment accountReportsFrag = null;

    private ArrayList<BackHandledFragment> fragments;

    public Fragment getMenuFragment() {
        if (menuFrag == null)
            menuFrag = new MenuFragment();
        return menuFrag;
    }
    public Fragment getAccountFrag() {
        if (accountFrag == null)
            accountFrag = new AccountsFragment();
        return accountFrag;
    }
    public Fragment getTransferFrag() {
        if (transferFrag == null)
            transferFrag = new TransferFragment();
        return transferFrag;
    }
    public Fragment getJuridicalFrag() {
        if (juridicalFrag == null)
            juridicalFrag = new JuridicalsFragment();
        return juridicalFrag;
    }
    public Fragment getAddCatFrag() {
        if (addCatFrag == null)
            addCatFrag = new AddCategoryFragment();
        return addCatFrag;
    }
    public Fragment getAddAccountFrag() {
        if (addAccountFrag == null)
            addAccountFrag = new AddAccountFragment();
        return addAccountFrag;
    }
    public Fragment getAddJUridicalFrag() {
        if (addJUridicalFrag == null)
            addJUridicalFrag = new AddJuridicalFragment();
        return addJUridicalFrag;
    }
    public Fragment getAddTransactionFrag() {
        if (addTransactionFrag == null)
            addTransactionFrag = new AddTransactionFragment();
        return addTransactionFrag;
    }
    public Fragment getAddTranactionCatFrag() {
        if (addTransactionCatFrag == null)
            addTransactionCatFrag = new AddTransActionCatFragment();
        return addTransactionCatFrag;
    }
    public Fragment getAccountReportsFrag() {
        if (accountReportsFrag == null)
            accountReportsFrag = new AccountReportsFragment();
        return accountReportsFrag;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        regFont = Typeface.createFromAsset(getAssets(), "fonts/Bahij_Nazanin_Regular.ttf");
        boldFont = Typeface.createFromAsset(getAssets(), "fonts/Bahij_Nazanin_Bold.ttf");

        fragments = new ArrayList<>();
        fragments.add(accountFrag);
        fragments.add(juridicalFrag);
        fragments.add(addTransactionFrag);
        fragments.add(transferFrag);

        dbHandler = DbHandler.getDbInstance(this);

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        dbHandler = DbHandler.getDbInstance(this);

        if(findViewById(R.id.container) != null) {
            transaction.add(R.id.container,getMenuFragment());
            transaction.commit();
        }

    }

    @Override
    public void setSelectedFragment(BackHandledFragment selectedFragment) {
        this.selectedFragment = selectedFragment;
    }

    @Override
    public void onBackPressed() {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        transaction.replace(R.id.container,getMenuFragment());
        transaction.commit();
        }

    @Override
    public void launchFragment(int fragNumber) {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        switch (fragNumber) {
            case 0:transaction.replace(R.id.container,getAccountFrag());
                break;
            case 1:transaction.replace(R.id.container,getJuridicalFrag());
                break;
            case 2:transaction.replace(R.id.container,getAddTransactionFrag());
                break;
            case 3:transaction.replace(R.id.container,getTransferFrag());
                break;
            case 4:transaction.replace(R.id.container,getAccountReportsFrag());
                break;


            default:;

        }
        transaction.addToBackStack("menu");
        transaction.commit();

    }

    @Override
    public void backToJuridicalsFrag() {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.container,getJuridicalFrag());
        transaction.commit();

    }

    @Override
    public void backToAccountsFrag() {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.container,getAccountFrag());
        transaction.commit();
    }

    @Override
    public void goToAddAccountFragment() {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.container,getAddAccountFrag());
        transaction.addToBackStack("account");
        transaction.commit();

    }

    @Override
    public void goToAddJurdicalFrag() {
        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.container,getAddJUridicalFrag());
        transaction.addToBackStack("juridical");
        transaction.commit();
    }

    @Override
    public void goToAddCategoryFrag() {
        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.container,getAddCatFrag());
        transaction.addToBackStack("juridical");
        transaction.commit();
    }

    @Override
    public void goToAddTransactionCatFrag() {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.container,getAddTranactionCatFrag());
        transaction.addToBackStack("add transaction");
        transaction.commit();

    }

    @Override
    public void backToAddTransactionFrag() {

        transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.container,getAddTranactionCatFrag());
        transaction.addToBackStack("add transaction");
        transaction.commit();

    }

    public void initDatas(){

        dbHandler = DbHandler.getDbInstance(this);
        dbHandler.removeAccounts();
        dbHandler.removeJuridicals();
        dbHandler.removeJuridicalsCats();
        dbHandler.removeTransactionCats();
        dbHandler.removeTransactions();
        dbHandler.removeTransfers();

        Random rnd = new Random();

        for(int i=0;i<6;i++) {
            Account account = new Account();
            account.setBalance(100000*rnd.nextInt(Integer.SIZE-1));
            account.setName("Account("+(i+1)+")");
            account.setDescription("this is my new account by number:"+i+1);
            dbHandler.addAccount(account);
        }

        ArrayList<Account> accounts = dbHandler.getAccounts();

        for(int i=0;i<6;i++) {
            JuridicalCategory category = new JuridicalCategory();
            category.setName("JurCategory("+(i+1)+")");
            category.setDescription("this is  juridical category by number:"+i+1);
            dbHandler.addJuridicalCat(category);
        }

        ArrayList<JuridicalCategory> jurCategories = dbHandler.getJuridicalCats();

        for(int i=0;i<6;i++) {
            Juridical juridical = new Juridical();
            juridical.setName("Juridical("+(i+1)+")");
            juridical.setPhone("091"+rnd.nextInt(Integer.SIZE-1)%9+"145"+rnd.nextInt(Integer.SIZE-1)%9+rnd.nextInt(Integer.SIZE-1)%9+"01");
            juridical.setEmail(juridical.getName()+"@gmail.com");
            juridical.setDescription("this is  juridical by number:"+i+1);
            juridical.setCategoryId(jurCategories.get(rnd.nextInt(Integer.SIZE-1)%jurCategories.size()).getId());
            dbHandler.addJuridical(juridical);
        }

        ArrayList<Juridical> juridicals = dbHandler.getJuridicals();

        for(int i=0;i<12;i++) {
            TransactionCategory tranCategory = new TransactionCategory();

            tranCategory.setName("Main_TranCat(" + (i + 1) + ")");

            if (rnd.nextBoolean())
                tranCategory.setType("هزینه");
            else
                tranCategory.setType("درآمد");

            dbHandler.addTransactionCat(tranCategory);
        }

        ArrayList<TransactionCategory> parentCats = dbHandler.getFirstLevelTransActionCats("هزینه");
        parentCats.addAll( ((List<TransactionCategory>)dbHandler.getFirstLevelTransActionCats("درآمد") ) );

        for(int i=0;i<12;i++) {
            TransactionCategory tranCategory = new TransactionCategory();

            tranCategory.setName("Sub_TranCat(" + (i + 1) + ")");

            if (rnd.nextBoolean())
                tranCategory.setType("هزینه");
            else
                tranCategory.setType("درآمد");

            tranCategory.setParentId( parentCats.get(rnd.nextInt(Integer.SIZE-1)%parentCats.size()).getId());

            dbHandler.addTransactionCat(tranCategory);
        }

    }

    @Override
    public void getReports(PersianDate date) {

    }
}
