package com.example.salehaf.accountingapp;

import java.util.Date;

import saman.zamani.persiandate.PersianDate;

/**
 * Created by salehaf on 1/26/2018.
 */

public class Transfer {

    private Integer id;
    private Integer amount;
    private Integer originAccountID;
    private Integer destAccountID;
    private PersianDate date;
    private String description;
    private Integer photoID;

    public Transfer() {

        this.date = new PersianDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getOriginAccountID() {
        return originAccountID;
    }

    public void setOriginAccountID(Integer originAccountID) {
        this.originAccountID = originAccountID;
    }

    public Integer getDestAccountID() {
        return destAccountID;
    }

    public void setDestAccountID(Integer destAccountID) {
        this.destAccountID = destAccountID;
    }

    public PersianDate getDate() {
        return date;
    }

    public void setDate(PersianDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPhotoID() {
        return photoID;
    }

    public void setPhotoID(Integer photoID) {
        this.photoID = photoID;
    }

    @Override
    public String toString() {
        return "date:"+this.date.getSecond()+"/"+this.amount;
    }
}
