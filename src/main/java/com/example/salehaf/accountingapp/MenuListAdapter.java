package com.example.salehaf.accountingapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by salehaf on 1/14/2018.
 */

public class MenuListAdapter extends BaseAdapter {

    private Context context;
    private String[]menuItemsName;
    private TypedArray menuItemsImage;

    public MenuListAdapter(Context context, String[] menuItemsName, TypedArray menuItemsImage) {
        this.context = context;
        this.menuItemsName = menuItemsName;
        this.menuItemsImage = menuItemsImage;
    }

    @Override
    public int getCount() {
        return menuItemsName.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_item,null);
        }

        TextView menuItemName = (TextView)convertView.findViewById(R.id.menu_item_title);
        menuItemName.setTypeface(MainActivity.regFont);
        menuItemName.setText(menuItemsName[position]);

        ImageView menuItemImage = (ImageView)convertView.findViewById(R.id.menu_item_image);
        Glide.with(context).load(menuItemsImage.getResourceId(position,-1)).into(menuItemImage);

        return convertView;
    }
}
