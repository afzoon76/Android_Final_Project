package com.example.salehaf.accountingapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by salehaf on 1/11/2018.
 */

public class JuridicalsFragment extends BackHandledFragment {


    private static final String TAG = "juridicalFragment";
    private Context context;
    private DbHandler dbHandler;
    private View view;
    private TextView title;
    private JuridicalsFragListener activityCommander;
    private ExpandableListView categorysList;
    private ExpandableListAdapter categoryAdapter;
    private ArrayList<JuridicalCategory> categories;
    private HashMap < JuridicalCategory,ArrayList<Juridical>> categoryHash;
    private Button addJuridicalButton;
    private Button addCategoryButton;


    public interface JuridicalsFragListener{

        void goToAddJurdicalFrag();
        void goToAddCategoryFrag();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            activityCommander = (JuridicalsFragListener) activity;
        }catch (ClassCastException e){

            throw new ClassCastException(activity.toString());

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.context = getActivity();
        view = inflater.inflate(R.layout.juridicals_fragment, container, false);
        title = (TextView)view.findViewById(R.id.juridicals_title);
        title.setTypeface(MainActivity.boldFont);
        addJuridicalButton = (Button)view.findViewById(R.id.add_juridical_frag_button);
        addCategoryButton = (Button)view.findViewById(R.id.add_category_frag_button);

        addJuridicalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommander.goToAddJurdicalFrag();
            }
        });
        addCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommander.goToAddCategoryFrag();
            }
        });


        categorysList = (ExpandableListView)view.findViewById(R.id.juridical_cat_list);
        dbHandler = DbHandler.getDbInstance(context);
        categoryHash = new HashMap<>();

        categories = dbHandler.getJuridicalCats();

        for(int i=0 ; i<categories.size() ; i++)
        {
            ArrayList<Juridical> juridicals = dbHandler.getCategoryJuridicals(categories.get(i).getId());
            categoryHash.put(categories.get(i),juridicals);
        }

        categoryAdapter = new CategoryListAdapter(context,this.categories,categoryHash);
        categorysList.setAdapter(categoryAdapter);


        return view;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void addCategory(String catName,String catDesc,String jurName,String jurPhone){

        JuridicalCategory category= new JuridicalCategory();
        category.setName(catName);
        category.setDescription(catDesc);

        dbHandler.addJuridicalCat(category);

        Juridical juridical = new Juridical();
        juridical.setId(category.getId());
        juridical.setName(jurName);
        juridical.setPhone(jurPhone);

        dbHandler.addJuridical(juridical);

    }
}

